# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='django_custommail',
    version='1.0.0',
    description='A simple django module for dealing with editable emails',
    long_description=long_description,
    url='https://git.eleves.ens.fr/cof-geek/django_custommail',
    author='Various',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python :: 3',
    ],
    packages=find_packages(),
    install_requires=['Django>=1.8'],
)
