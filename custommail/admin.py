"""
Administration of custom mails.
This admin site is designed for users with the ability to edit emails.
There is no way to change variables and variable types yet.
"""

from django import forms
from django.contrib import admin
from django.template import Context, Template
from django.utils.translation import ugettext_lazy as _

from .models import CustomMail, Identity, Type, Variable

###
# Types
###


def _has_scheme(data, scheme):
    return all([
        (data['content_type'] is not None) is scheme[0],
        (data['inner1'] is not None) is scheme[1],
        (data['inner2'] is not None) is scheme[2]
    ])


class TypeForm(forms.ModelForm):
    """
    Ensures that the types created in the admin site are well-formed.
    """

    def clean(self):
        cleaned_data = super(TypeForm, self).clean()
        kind = cleaned_data['kind']
        if kind == 'int':
            if not _has_scheme(cleaned_data, [False, False, False]):
                raise(forms.ValidationError(
                    _("int should have no attributes"),
                    code='invalid_int'
                ))
        elif kind == 'model':
            if not _has_scheme(cleaned_data, [True, False, False]):
                raise(forms.ValidationError(
                    _("model should have nothing but a content_type"),
                    code='invalid_model'
                ))
        elif kind == 'list':
            if not _has_scheme(cleaned_data, [False, True, False]):
                raise(forms.ValidationError(
                    _("list should have nothing but a inner1"),
                    code='invalid_list'
                ))
        elif kind == 'pair':
            if not _has_scheme(cleaned_data, [False, True, True]):
                raise(forms.ValidationError(
                    _("pair should have an inner1, an inner2 and "
                      "no content_type"),
                    code='invalid_pair'
                ))
        return cleaned_data

    class Meta:
        model = Type
        exclude = []


class TypeAdmin(admin.ModelAdmin):
    """
    Allow types creation with some validity checks.
    """
    form = TypeForm


###
# Variables
###


class VariableInline(admin.TabularInline):
    """
    An inline class for variables intended to be included in CustomMail's
    ModelAdmin.
    """
    model = Variable


###
# Custom mails
###


class CustomMailForm(forms.ModelForm):
    """
    Displays informations about variables and checks the template for errors.
    """
    variables_info = forms.CharField(
        required=False,
        widget=forms.Textarea(attrs={'rows': 30, 'readonly': True}),
    )
    example = forms.CharField(
        required=False,
        widget=forms.Textarea(attrs={'rows': 30, 'readonly': True}),
        help_text=_("Save to refresh this example."),
    )

    def __init__(self, *args, **kwargs):
        instance = kwargs.pop('instance', None)
        initial = kwargs.pop('initial', {})
        if instance:
            initial['variables_info'] = instance.get_variables_info()
            subject, body = instance.render(instance.get_random_context())
            initial['example'] = '[Subject: {:s}]\n{:s}'.format(subject, body)
        super(CustomMailForm, self).__init__(
            instance=instance, initial=initial, *args, **kwargs)

    def clean(self):
        """
        Does the rendering check and displays the results.
        """
        cleaned_data = super(CustomMailForm, self).clean()
        context = Context(self.instance.get_random_context())
        try:
            Template(cleaned_data['subject']).render(context)
            Template(cleaned_data['body']).render(context)
        except Exception as error:
            raise forms.ValidationError(
                _("Error %(error)"),
                code='rendering_fails',
                params={'error': str(error)}
            )
        return cleaned_data

    class Meta:
        widgets = {
            'body': forms.Textarea(attrs={'rows': 30}),
        }


class CustomMailAdmin(admin.ModelAdmin):
    """
    The admin interface for custom mails provides testing features :
      - At form validation tests are run to checks that the rendering doesn't
        fail. If an error occur, the form doesn't validate and the error is
        shown to the user.
      - An example email (filled with random data) is displayed at the bottom
        of the change form.
    """
    list_display = ['shortname', 'subject']
    search_fields = ['shortname', 'subject', 'description']

    fieldsets = [
        (None, {
            'fields': ['shortname', 'description'],
        }),
        (_("Headers"), {
            'fields': ['from_email', 'reply_to', 'cc', 'bcc'],
        }),
        (_("Content"), {
            'fields': ['subject', 'body', 'variables_info'],
        }),
        (None, {
            'fields': ['example'],
        }),
    ]
    form = CustomMailForm
    inlines = [VariableInline]

    def get_readonly_fields(self, request, obj=None):
        """Only allows email creators to edit the shortname"""
        if request.user.has_perm('custommail.add_custommail'):
            return []
        else:
            return ['shortname']


admin.site.register(Type, TypeAdmin)
admin.site.register(CustomMail, CustomMailAdmin)
admin.site.register(Identity)
