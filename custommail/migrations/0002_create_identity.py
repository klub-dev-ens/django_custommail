# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('custommail', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='custommail',
            options={
                'verbose_name': 'customizable email',
                'verbose_name_plural': 'customizable emails',
            },
        ),
        migrations.AlterField(
            model_name='custommail',
            name='body',
            field=models.TextField(verbose_name='body'),
        ),
        migrations.AlterField(
            model_name='custommail',
            name='description',
            field=models.TextField(verbose_name='description', blank=True),
        ),
        migrations.AlterField(
            model_name='custommail',
            name='shortname',
            field=models.SlugField(
                unique=True, verbose_name='unique identifier'
            ),
        ),
        migrations.AlterField(
            model_name='custommail',
            name='subject',
            field=models.CharField(max_length=200, verbose_name='subject'),
        ),
        migrations.CreateModel(
            name='Identity',
            fields=[
                ('id', models.AutoField(
                    auto_created=True, verbose_name='ID', serialize=False,
                    primary_key=True,
                )),
                ('name', models.CharField(
                    max_length=255, blank=True, verbose_name='name',
                )),
                ('email', models.EmailField(
                    max_length=254, verbose_name='email',
                )),
            ],
            options={
                'verbose_name': 'identity',
                'verbose_name_plural': 'identities',
            },
        ),
        migrations.AddField(
            model_name='custommail',
            name='bcc',
            field=models.ManyToManyField(
                related_name='_custommail_bcc_+', verbose_name='bcc',
                blank=True, to='custommail.Identity',
            ),
        ),
        migrations.AddField(
            model_name='custommail',
            name='cc',
            field=models.ManyToManyField(
                related_name='_custommail_cc_+', verbose_name='cc', blank=True,
                to='custommail.Identity',
            ),
        ),
        migrations.AddField(
            model_name='custommail',
            name='from_email',
            field=models.ForeignKey(
                related_name='+', blank=True, null=True,
                to='custommail.Identity', verbose_name='from',
                on_delete=models.SET_NULL,
            ),
        ),
        migrations.AddField(
            model_name='custommail',
            name='reply_to',
            field=models.ManyToManyField(
                related_name='_custommail_reply_to_+', verbose_name='reply to',
                blank=True, to='custommail.Identity',
            ),
        ),
    ]
