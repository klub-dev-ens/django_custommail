# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomMail',
            fields=[
                (
                    'id',
                    models.AutoField(
                        serialize=False, verbose_name='ID', primary_key=True,
                        auto_created=True
                    )
                ),
                (
                    'shortname',
                    models.SlugField(
                        verbose_name='Unique identifier', unique=True
                    )
                ),
                (
                    'subject',
                    models.CharField(
                        verbose_name="Email's subject", max_length=200
                    )
                ),
                (
                    'body',
                    models.TextField(verbose_name="Email's body")
                ),
                (
                    'description',
                    models.TextField(
                        verbose_name='Short description', blank=True
                    )
                ),
            ],
            options={
                'verbose_name': 'Customizable email',
                'verbose_name_plural': 'Customizable emails',
            },
        ),
        migrations.CreateModel(
            name='Type',
            fields=[
                (
                    'id',
                    models.AutoField(
                        serialize=False, verbose_name='ID', primary_key=True,
                        auto_created=True
                    )
                ),
                (
                    'kind',
                    models.CharField(
                        max_length=5,
                        choices=[
                            ('int', 'Integer'), ('list', 'List'),
                            ('pair', 'Pair'), ('model', 'Django model')
                        ]
                    )
                ),
                (
                    'content_type',
                    models.ForeignKey(
                        on_delete=models.PROTECT,
                        to='contenttypes.ContentType', null=True, blank=True
                    )
                ),
                (
                    'inner1',
                    models.ForeignKey(
                        on_delete=models.PROTECT,
                        to='custommail.Type',
                        null=True, related_name='+', blank=True
                    )
                ),
                (
                    'inner2',
                    models.ForeignKey(
                        on_delete=models.PROTECT,
                        to='custommail.Type',
                        null=True, related_name='+', blank=True
                    )
                ),
            ],
            options={
                'verbose_name': 'Type',
                'verbose_name_plural': 'Types',
            },
        ),
        migrations.CreateModel(
            name='Variable',
            fields=[
                (
                    'id',
                    models.AutoField(
                        serialize=False, verbose_name='ID', primary_key=True,
                        auto_created=True
                    )
                ),
                (
                    'name',
                    models.CharField(max_length=100)
                ),
                (
                    'description',
                    models.CharField(max_length=1024)
                ),
                (
                    'custommail',
                    models.ForeignKey(
                        on_delete=models.CASCADE,
                        related_name='variables', to='custommail.CustomMail'
                    )
                ),
                (
                    'type',
                    models.ForeignKey(
                        on_delete=models.CASCADE,
                        related_name='related_variables', to='custommail.Type'
                    )
                ),
            ],
            options={
                'verbose_name': 'Typed variable',
                'verbose_name_plural': 'Typed variables',
            },
        ),
    ]
