import json

from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand

from custommail.models import CustomMail, Type, Variable


def read_files(filenames):
    """Read a list of json files and concatenate the results"""
    json_data = []
    for filename in filenames:
        with open(filename, 'r') as file:
            json_data += json.load(file)
    return json_data


class Command(BaseCommand):
    help = (
        "Read serialized custommail objects from one or more json files. The "
        "serialization format is the one produced by "
        "`python manage.py dumpdata custommail --natural-foreign'. "
        "Thus, a good way to update these files is to load them using the "
        "`syncmails` commmand, make your modifications via the admin site "
        "and run `dumpdata` again as explained above."
    )

    def add_arguments(self, parser):
        parser.add_argument("filenames", nargs='+', metavar="FILE")

    def handle(self, *args, **options):
        # Mappings pk -> objects
        assoc = {'types': {}, 'mails': {}}
        # For display purpose
        status = {'synced': 0, 'unchanged': 0}

        json_data = read_files(options["filenames"])

        for obj in json_data:
            fields = obj['fields']

            # Variable types
            if obj['model'] == 'custommail.type':
                fields['inner1'] = assoc['types'].get(fields['inner1'])
                fields['inner2'] = assoc['types'].get(fields['inner2'])
                if fields['kind'] == 'model':
                    fields['content_type'] = (
                        ContentType.objects
                        .get_by_natural_key(*fields["content_type"])
                    )
                var_type, _ = Type.objects.get_or_create(**fields)
                assoc['types'][obj['pk']] = var_type

            # Custom mails
            if obj['model'] == 'custommail.custommail':
                mail = None
                try:
                    mail = CustomMail.objects.get(
                        shortname=fields["shortname"])
                    status["unchanged"] += 1
                except CustomMail.DoesNotExist:
                    # XXX. headers using a m2m relation are not supported yet
                    m2m_fields = ["cc", "bcc", "reply_to"]
                    for field_name in m2m_fields:
                        assert fields.get(field_name, []) == []
                    fields = {
                        field_name: value
                        for field_name, value in fields.items()
                        if field_name not in m2m_fields
                    }
                    mail = CustomMail.objects.create(**fields)
                    status['synced'] += 1
                    if options["verbosity"]:
                        self.stdout.write(
                            "SYNCED {}".format(fields["shortname"])
                        )
                assoc['mails'][obj['pk']] = mail

            # Variables
            if obj['model'] == 'custommail.variable':
                fields['custommail'] = assoc['mails'].get(fields['custommail'])
                fields['type'] = assoc['types'].get(fields['type'])
                try:
                    Variable.objects.get(
                        custommail=fields['custommail'],
                        name=fields['name']
                    )
                except Variable.DoesNotExist:
                    Variable.objects.create(**fields)

        if options["verbosity"]:
            self.stdout.write(
                '{synced:d} mails synchronized {unchanged:d} unchanged'
                .format(**status)
            )
