from django.apps import AppConfig


class CustomMailConfig(AppConfig):
    name = 'custommail'
    verbose_name = 'Mails personnalisables'
