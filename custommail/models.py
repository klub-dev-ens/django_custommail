"""
Models of the custommail project.

- `CustomMail` stores email templates.
- The other models provide tools for testing emails before saving them and
  therefore prevent errors.
"""

import random

from django.contrib.contenttypes.models import ContentType
from django.core.mail import EmailMultiAlternatives
from django.db import models
from django.template import Context, Template
from django.utils.translation import ugettext_lazy as _


class Type(models.Model):
    """
    The types of variables used in emails, they are used for testing purposes.
    The available types are :
    - Integers (int)
    - Models (models) : in that case, a content type must be specified.
    - Lists (list) : in that case, the type of the elements of the list must be
      specified in `self.inner1`.
    - Pairs (pair) : similar to lists but there have two underlying types
      `self.inner1` and `self.inner2`.
    """

    AVAILABLE_TYPES = [
        ('int', _("Integer")),
        ('list', _("List")),
        ('pair', _("Pair")),
        ('model', _("Django model")),
    ]

    kind = models.CharField(choices=AVAILABLE_TYPES, max_length=5)
    content_type = models.ForeignKey(
        ContentType,
        on_delete=models.PROTECT,
        blank=True, null=True
    )
    inner1 = models.ForeignKey(
        'self',
        on_delete=models.PROTECT,
        related_name='+',
        blank=True, null=True
    )
    inner2 = models.ForeignKey(
        'self',
        on_delete=models.PROTECT,
        related_name='+',
        blank=True, null=True
    )

    def get_random_sample(self):
        """
        Returns some random sample data of the right type :
        - For integers, a random integer
        - For models, a random instance of the object
        - list and pairs are treated recursively
        """
        if self.kind == 'int':
            return random.randint(0, 10)
        elif self.kind == 'model':
            klass = self.content_type.model_class()
            return klass.objects.order_by('?').first()
        elif self.kind == 'list':
            return [self.inner1.get_random_sample() for x in range(3)]
        elif self.kind == 'pair':
            return (self.inner1.get_random_sample(),
                    self.inner2.get_random_sample())

    def __str__(self):
        templates = {
            'int': 'int',
            'list': '{typ1!s} list',
            'pair': '({typ1!s}, {typ2!s})',
            'model': "'{contenttype!s}' model"
        }
        return templates[self.kind].format(
            typ1=self.inner1,
            typ2=self.inner2,
            contenttype=self.content_type)

    class Meta:
        verbose_name = _("Type")
        verbose_name_plural = _("Types")


class Identity(models.Model):
    name = models.CharField(
        _("name"),
        max_length=255, blank=True,
    )
    email = models.EmailField(_("email"))

    class Meta:
        verbose_name = _("identity")
        verbose_name_plural = _("identities")

    def __str__(self):
        if self.name:
            return '{} <{}>'.format(self.name, self.email)
        return str(self.email)


class CustomMail(models.Model):
    """
    A custom mail is a template stored in the databases with some additional
    data such as the variables used in it or a short description.
    """
    shortname = models.SlugField(
        _("unique identifier"),
        max_length=50, unique=True,
    )
    description = models.TextField(_("description"), blank=True)

    subject = models.CharField(_("subject"), max_length=200)
    body = models.TextField(_("body"))

    from_email = models.ForeignKey(
        'Identity', on_delete=models.SET_NULL,
        verbose_name=_("from"),
        blank=True, null=True, related_name='+',
    )
    cc = models.ManyToManyField(
        'Identity',
        verbose_name=_("cc"),
        blank=True, related_name='+',
    )
    bcc = models.ManyToManyField(
        'Identity',
        verbose_name=_("bcc"),
        blank=True, related_name='+',
    )
    reply_to = models.ManyToManyField(
        'Identity',
        verbose_name=_("reply to"),
        blank=True, related_name='+',
    )

    class Meta:
        verbose_name = _("customizable email")
        verbose_name_plural = _("customizable emails")

    def __str__(self):
        return "{:s}: {:s}".format(self.shortname, self.subject)

    def get_variables_info(self):
        """
        Returns an informative text about the variables required to render the
        email.
        """
        return '\n'.join([
            '{:s} : {!s}\n\t{:s}'.format(var.name, var.type, var.description)
            for var in self.variables.all()
        ])

    def get_random_context(self):
        """
        Generates an random exemple rendering context.
        """
        return {
            var.name: var.type.get_random_sample()
            for var in self.variables.all()
        }

    def render(self, context):
        """
        Generates the email using the given context.
        """
        subject = Template(self.subject)
        body = Template(self.body)
        context = Context(context)
        return (subject.render(context), body.render(context))

    def get_message(self, context=None, *args, **kwargs):
        if context is None:
            context = {}

        subject, body = self.render(context)

        if self.from_email:
            kwargs['from_email'] = str(self.from_email)

        cc = self.cc.all()
        if cc:
            kwargs['cc'] = list(kwargs['cc']) + list(map(str, cc))

        bcc = self.bcc.all()
        if bcc:
            kwargs['bcc'] = list(kwargs['bcc']) + list(map(str, bcc))

        reply_to = self.reply_to.all()
        if reply_to:
            kwargs['reply_to'] = (
                list(kwargs['reply_to']) + list(map(str, reply_to)))

        return EmailMultiAlternatives(subject, body, *args, **kwargs)


class Variable(models.Model):
    """
    The variables used in an email. They are related to only one custommail
    object and must have a type.
    """
    custommail = models.ForeignKey(
        CustomMail,
        on_delete=models.CASCADE,
        related_name='variables'
    )
    type = models.ForeignKey(
        Type,
        on_delete=models.CASCADE,
        related_name='related_variables'
    )
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1024)

    class Meta:
        verbose_name = _("Typed variable")
        verbose_name_plural = _("Typed variables")
