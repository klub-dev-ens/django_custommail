"""
Utility functions for randering and sending custom mails.
"""

from django.core.mail import send_mail, send_mass_mail

from .models import CustomMail


def render_custom_mail(shortname, context):
    """
    Returns the custom mail which shortname is `shortname` as a couple of
    strings:
        - The mail object
        - The mail body
    It is rendered with the context data contained in `context`.
    Raises `CustomMail.DoesNotExist` if no CustomMail is found.
    """
    mail = CustomMail.objects.get(shortname=shortname)
    return mail.render(context)


def send_custom_mail(shortname, *args, context=None, **kwargs):
    """
    Sends a custom mail using the template referenced by `shortname` rendered
    with `context`.
    The other arguments are similar to django's send_mail.

    """
    if context is None:
        context = {}

    cm = CustomMail.objects.get(shortname=shortname)

    subject, body = cm.render(context)

    if cm.from_email:
        kwargs['from_email'] = str(cm.from_email)

    return send_mail(subject, body, *args, **kwargs)


def send_mass_custom_mail(datatuple, *args, **kwargs):
    """
    Similar to send_custom_mail but optimized for sending several emails:
    `datatuple` is a tuple in which each element is in this format:
        (shortname, context, from_email, recipient_list)

    `from_email` is replaced by the `CustomMail` one, if any.
    """
    # We load the CustomMail objects only once
    qset = CustomMail.objects.filter(shortname__in=[
        shortname for shortname, _, _, _ in datatuple])
    mails = {mail.shortname: mail for mail in qset}

    django_datatuple = []
    for shortname, context, from_email, recipient_list in datatuple:
        if context is None:
            context = {}
        cm = mails[shortname]
        subject, body = cm.render(context)
        if cm.from_email:
            from_email = str(cm.from_email)
        django_datatuple.append(
            (subject, body, from_email, recipient_list))
    return send_mass_mail(django_datatuple, *args, **kwargs)
