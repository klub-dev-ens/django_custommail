import os

from django.contrib.contenttypes.models import ContentType
from django.core.management import call_command
from django.test import TestCase

from custommail.models import CustomMail, Type, Variable

HERE = os.path.dirname(__file__)


def transform_var(var):
    return (var.name, var.custommail.shortname, str(var.type))


def transform_type(ty):
    return str(ty)


def transform_email(email):
    return (
        email.shortname,
        email.subject,
        email.body,
        tuple(transform_var(var) for var in email.variables.all())
    )


class SyncMailsTest(TestCase):
    TEST_DATA = os.path.join(HERE, "data", "custommail.json")

    GENERATED_FILE = os.path.join(HERE, "data", "__test_stuff.json")

    def test_sync(self):
        """Read mail data is loaded correctly"""
        call_command("syncmails", self.TEST_DATA, "--verbosity=0")
        # Types
        expected_types = [
            "int", "(int, int)", "(int, int) list", "'permission' model"
        ]
        self.assertQuerysetEqual(
            Type.objects.all(), expected_types,
            transform=transform_type, ordered=False
        )

        # Variables
        expected_variables = [
            ("my_list", "test-email", "(int, int) list"),
            ("perm", "test-email", "'permission' model")
        ]
        self.assertQuerysetEqual(
            Variable.objects.all(), expected_variables,
            transform=transform_var, ordered=False
        )

        # Mails
        mail = CustomMail.objects.get()  # there should be only one mail
        expected_vars = [transform_var(var) for var in Variable.objects.all()]
        self.assertQuerysetEqual(
            mail.variables.all(), expected_vars,
            ordered=False, transform=transform_var
        )

    def test_dump_and_load(self):
        """
        Dumping all custommail objects, flushing the database and loading
        them back again should be leave us in the same state
        """
        # Remove the generated dump after the test
        def rm_generated():
            if os.path.exists(self.GENERATED_FILE):
                os.remove(self.GENERATED_FILE)

        self.addCleanup(rm_generated)

        # Generate objects
        ty_int = Type.objects.create(kind="int")
        ty_int_l = Type.objects.create(kind="list", inner1=ty_int)
        ty_pair = Type.objects.create(
            kind="pair", inner1=ty_int, inner2=ty_int_l
        )
        ty_ct = Type.objects.create(
            kind="model",
            content_type=ContentType.objects.first()
        )

        mail = CustomMail.objects.create(
            shortname="test-email",
            subject="subject",
            body="I {{ v_int }}\nL {{ v_list }}\nP {{ v_pair }}\nCT {{ v_ct }}"
        )

        Variable.objects.create(custommail=mail, name="v_int", type=ty_int)
        Variable.objects.create(custommail=mail, name="v_list", type=ty_int_l)
        Variable.objects.create(custommail=mail, name="v_pair", type=ty_pair)
        Variable.objects.create(custommail=mail, name="v_ct", type=ty_ct)

        all_types = [transform_type(ty) for ty in Type.objects.all()]
        all_vars = [transform_var(var) for var in Variable.objects.all()]
        all_mails = [transform_email(m) for m in CustomMail.objects.all()]

        # Dump
        call_command(
            "dumpdata", "custommail",
            "--indent=4",
            "--natural-foreign",
            "--output={}".format(self.GENERATED_FILE)
        )

        # Flush tables
        CustomMail.objects.all().delete()
        Variable.objects.all().delete()
        for ty in Type.objects.order_by("-id"):
            # Must be deleted in reverse order because of fk constraints
            ty.delete()

        # Reload and check equality
        call_command("syncmails", self.GENERATED_FILE, "--verbosity=0")
        self.assertQuerysetEqual(
            Type.objects.all(), all_types,
            ordered=False, transform=transform_type
        )
        self.assertQuerysetEqual(
            Variable.objects.all(), all_vars,
            ordered=False, transform=transform_var
        )
        self.assertQuerysetEqual(
            CustomMail.objects.all(), all_mails,
            ordered=False, transform=transform_email
        )
