# -*- coding: utf-8 -*-
SECRET_KEY = 'iamacat'

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',

    'custommail',
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
    },
}

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
    },
]

ROOT_URLCONF = 'tests.urls'
