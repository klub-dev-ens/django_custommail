from django.contrib.auth import get_user_model
from django.core import mail
from django.test import TestCase

from custommail.models import CustomMail, Identity
from custommail.shortcuts import (
    render_custom_mail, send_custom_mail, send_mass_custom_mail,
)

User = get_user_model()


class RenderCustomMailTests(TestCase):

    def setUp(self):
        self.cm = CustomMail.objects.create(
            shortname='cmail1', subject="{{ n }}",
            body="{{ n }} objects",
        )

    def test(self):
        subject, body = render_custom_mail('cmail1', {'n': 3})
        self.assertEqual(subject, "3")
        self.assertEqual(body, "3 objects")


class SendCustomMailTests(TestCase):

    def setUp(self):
        self.cm = CustomMail.objects.create(
            shortname='cmail1', subject="{{ n }}",
            body="{{ n }} objects",
        )

        self.ident = Identity.objects.create(
            name='The Name', email='name@mail.net')

        self.ctx = {'n': 3}

    def test(self):
        """
        It renders the subject and body using the named CustomMail, then call
        the django' `send_mail` function.
        """
        send_custom_mail(
            'cmail1', context=self.ctx, from_email='root@local',
            recipient_list=['mail1@mail.net'],
        )

        self.assertEqual(len(mail.outbox), 1)
        msg = mail.outbox[0]
        self.assertEqual(msg.from_email, 'root@local')
        self.assertListEqual(msg.to, ['mail1@mail.net'])
        self.assertEqual(msg.subject, "3")
        self.assertEqual(msg.body, "3 objects")

    def test_additional_headers(self):
        """
        If any, `from_email` value of the `CustomMail` is used for the
        `send_mail` call.
        """
        self.cm.from_email = self.ident
        self.cm.save()

        send_custom_mail(
            'cmail1', context=self.ctx, recipient_list=['mail1@mail.net'])

        self.assertEqual(len(mail.outbox), 1)
        msg = mail.outbox[0]
        self.assertEqual(msg.from_email, str(self.ident))


class SendMassCustomMailTests(TestCase):

    def setUp(self):
        self.user = User(
            username='user', first_name='first', last_name='last')

        self.cm1 = CustomMail.objects.create(
            shortname='cmail1', subject="{{ n }}",
            body="{{ n }} objects",
        )
        self.ctx1 = {'n': 3}

        self.cm2 = CustomMail.objects.create(
            shortname='cmail2', subject="{{ user.get_short_name }}",
            body="{{ user.get_full_name }}",
        )
        self.ctx2 = {'user': self.user}

    def test(self):
        send_mass_custom_mail([
            ('cmail1', self.ctx1, 'root@local', ['mail1@mail.net']),
            ('cmail2', self.ctx2, 'root@local', ['mail2@mail.net']),
        ])

        self.assertEqual(len(mail.outbox), 2)
        msg1, msg2 = mail.outbox[0], mail.outbox[1]
        self.assertEqual(msg1.from_email, 'root@local')
        self.assertEqual(msg1.to, ['mail1@mail.net'])
        self.assertEqual(msg1.subject, "3")
        self.assertEqual(msg1.body, "3 objects")
        self.assertEqual(msg2.from_email, 'root@local')
        self.assertEqual(msg2.to, ['mail2@mail.net'])
        self.assertEqual(msg2.subject, "first")
        self.assertEqual(msg2.body, "first last")

    def test_num_queries(self):
        """CustomMail objects are loaded once."""
        with self.assertNumQueries(1):
            send_mass_custom_mail([
                ('cmail1', self.ctx1, 'root@local', ['mail1@mail.net']),
                ('cmail2', self.ctx2, 'root@local', ['mail2@mail.net']),
                ('cmail1', self.ctx1, 'root@local', ['mail3@mail.net']),
            ])
        self.assertEqual(len(mail.outbox), 3)

    def test_additional_headers(self):
        ident = Identity.objects.create(
            name='The Name', email='identity@mail.net')

        self.cm1.from_email = ident
        self.cm1.save()
        self.cm2.from_email = ident
        self.cm2.save()

        send_mass_custom_mail([
            ('cmail1', self.ctx1, 'root@local', ['mail1@mail.net']),
            ('cmail2', self.ctx2, None, ['mail2@mail.net']),
        ])

        self.assertEqual(len(mail.outbox), 2)
        msg1, msg2 = mail.outbox[0], mail.outbox[1]
        self.assertEqual(msg1.from_email, str(ident))
        self.assertEqual(msg2.from_email, str(ident))
