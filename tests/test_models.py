from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.core.mail import EmailMessage
from django.test import TestCase

from custommail.models import CustomMail, Identity, Type

User = get_user_model()


class CustomMailTests(TestCase):

    def setUp(self):
        self.user = User(
            username='user', email='user@mail.net',
            first_name='first', last_name='last',
        )

        self.ident1 = Identity.objects.create(
            name='Ident 1', email='ident1@mail.net')
        self.ident2 = Identity.objects.create(
            name='Ident 3', email='ident2@mail.net')
        self.ident3 = Identity.objects.create(
            name='Ident 3', email='ident3@mail.net')
        self.ident4 = Identity.objects.create(
            name='Ident 4', email='ident4@mail.net')

        self.t_user = Type.objects.create(
            kind='model', content_type=ContentType.objects.get_for_model(User))
        self.t_int = Type.objects.create(kind='int')

        self.cm = CustomMail.objects.create(
            shortname='cmail1', subject='{{ user.get_short_name }}',
            body=(
                "Greetings {{ user.get_full_name }},\n\n"
                "You have {{ c }} new notifications.\n\n"
                "Bye"
            ),
            description="Sent on new notifications",
        )
        self.ctx = {
            'user': self.user,
            'c': 3,
        }
        self.vars_data = [
            {'name': 'user', 'type': self.t_user},
            {'name': 'c', 'type': self.t_int, 'description': 'Notif counter'},
        ]

    def create_variables(self, cm, vars_data):
        for data in vars_data:
            cm.variables.create(**data)

    def test_str(self):
        self.assertEqual(str(self.cm), "cmail1: {{ user.get_short_name }}")

    def test_render(self):
        cm, ctx = self.cm, self.ctx

        subject, body = cm.render(ctx)

        self.assertEqual(subject, 'first')
        self.assertEqual(body, (
            "Greetings first last,\n\n"
            "You have 3 new notifications.\n\n"
            "Bye"
        ))

    def test_get_message(self):
        cm, ctx = self.cm, self.ctx

        msg = cm.get_message(ctx)

        self.assertIsInstance(msg, EmailMessage)
        self.assertEqual(msg.subject, 'first')
        self.assertEqual(msg.body, (
            "Greetings first last,\n\n"
            "You have 3 new notifications.\n\n"
            "Bye"
        ))

        # Additional positional and keywords arguments are given to the
        # EmailMessage constructor.
        msg = cm.get_message(ctx, reply_to=['reply@mail.net'])

        self.assertListEqual(msg.reply_to, ['reply@mail.net'])

        # From, Cc, Bcc and Reply-to are also retrieved from instance.
        # Instance' From takes precedence over args.
        # Instance' Cc, Bcc, Reply-to are appended to args.
        cm.from_email = self.ident1
        cm.cc = [self.ident2]
        cm.bcc = [self.ident3]
        cm.reply_to = [self.ident4]
        cm.save()

        msg = cm.get_message(
            ctx,
            from_email='from@mail.net',
            cc=['cc@mail.net'],
            bcc=['bcc@mail.net'],
            reply_to=['reply@mail.net'],
        )

        self.assertEqual(msg.from_email, str(self.ident1))
        self.assertCountEqual(msg.cc, [
            str(self.ident2), 'cc@mail.net'])
        self.assertCountEqual(msg.bcc, [
            str(self.ident3), 'bcc@mail.net'])
        self.assertCountEqual(msg.reply_to, [
            str(self.ident4), 'reply@mail.net'])

    def test_get_variables_info(self):
        cm = self.cm

        vars_info = cm.get_variables_info()
        self.assertEqual(vars_info, "")

        self.create_variables(cm, self.vars_data)

        vars_info = cm.get_variables_info()
        self.assertEqual(vars_info, (
            "user : 'user' model\n"
            "\t"
            "\n"
            "c : int\n"
            "\tNotif counter"
        ))

    def test_get_random_context(self):
        cm = self.cm

        rd_ctx = cm.get_random_context()
        self.assertDictEqual(rd_ctx, {})

        self.create_variables(cm, self.vars_data)
        self.user.save()

        rd_ctx = cm.get_random_context()
        self.assertIsInstance(rd_ctx['user'], User)
        self.assertIsInstance(rd_ctx['c'], int)


class IdentityTests(TestCase):

    def setUp(self):
        self.ident = Identity.objects.create(
            name='The Name', email='name@mail.net')

    def test_str(self):
        self.assertEqual(str(self.ident), 'The Name <name@mail.net>')

        self.ident.name = ''
        self.ident.save()

        self.assertEqual(str(self.ident), 'name@mail.net')


class TypeTests(TestCase):

    def setUp(self):
        self.t_int = Type.objects.create(kind='int')
        self.t_int_list = Type.objects.create(
            kind='list', inner1=self.t_int)
        self.t_int_list_int_pair = Type.objects.create(
            kind='pair', inner1=self.t_int_list, inner2=self.t_int)
        self.t_user = Type.objects.create(
            kind='model', content_type=ContentType.objects.get_for_model(User))

    def test_str(self):
        self.assertEqual(str(self.t_int), "int")
        self.assertEqual(str(self.t_int_list), "int list")
        self.assertEqual(str(self.t_int_list_int_pair), "(int list, int)")
        self.assertEqual(str(self.t_user), "'user' model")

    def test_get_random_sample(self):
        rd_int = self.t_int.get_random_sample()
        self.assertIsInstance(rd_int, int)

        rd_int_list = self.t_int_list.get_random_sample()
        self.assertIsInstance(rd_int_list, list)
        self.assertIsInstance(rd_int_list[0], int)

        rd_int_list_int_pair = self.t_int_list_int_pair.get_random_sample()
        self.assertEqual(len(rd_int_list_int_pair), 2)
        self.assertIsInstance(rd_int_list_int_pair[0], list)
        self.assertIsInstance(rd_int_list_int_pair[0][0], int)
        self.assertIsInstance(rd_int_list_int_pair[1], int)

        rd_user = self.t_user.get_random_sample()
        self.assertIsNone(rd_user)

        User.objects.create(username='user')

        rd_user = self.t_user.get_random_sample()
        self.assertIsInstance(rd_user, User)
