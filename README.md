# Django-Custom-Mail

django-custom-mail aims at providing tools to deal with editable templated
emails. The emails templates are stored in Django's database and can be edited
in the admin interface.

Tested with Django 1.8 - 1.11 (Python 3 only).

django-custom-mail also provides a check each time an email is modified to
guaranty that the rendering won't fail in a future use. The idea is the
following : the required emails of a Django app are loaded into the database by
the developers (during a data migration for example) with the specification of
all of their variables. The users allowed to edit an email can see the variables
but cannot modify them and they are used by the checker to ensure the email is
rendered correctly and without raising errors. An example email is also
displayed to the user.


## Example

```python
from django.contrib.contenttypes.models import ContentType
from custommail.models import Type, CustomMail, Variable
from custommail.utils import send_custom_mail

# Creating a new email
mail = CustomMail.objects.create(
    shortname='test_email',
    title='Hello {{ user.email }}',
    content='Hey, here is my complecated variable : {{ var }}'
)

# Defining some useful types
integer = Type.objects.create(kind='int')
int_list = Type.objects.create(kind='list', inner1=integer)
couple = Type.objects.create(kind='pair', inner1=integer, inner2=int_list)
couple_list = Type.objects.create(kind='list', inner1=couple)
user_type = Type.objects.create(
    kind='model',
    content_type=ContentType.objects.get(app_label='auth', model='user')
)

# Defining a couple of variables used in the email above
Variable.objects.create(
    custommail=mail,
    type=couple_list,
    name='var',
    description='A list of couples of integers and integers list'
)
Variable.objects.create(
    custommail=mail,
    type=user_type,
    name='user',
    description='A django user'
)


# Sending the email
send_custom_mail(
    "test_email",                       # shortname of custommail
    context=mail.get_random_context()   # the context
    from_email="root@example.com",      # other arguments passed to django's
    recipient_list=["foo@example.com"]  # sendmail
)
```    
Here we use random data but you are supposed to user an appropriate context like
`{'user': request.user, 'var': [(42, [13]), (0, [1, 2, 3])]}`


## Tests

To run tests for all supported environments:

```bash
$ tox
```

## License

This module is released under the [MIT](https://opensource.org/licenses/MIT)
license
